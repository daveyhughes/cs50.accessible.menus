define(function(require, exports, module) {
    main.consumes = [
        "Plugin", "ace", "ace.status", "auth", "commands", "console", "Divider",
        "immediate", "keymaps", "layout", "Menu", "MenuItem", "menus", "mount",
        "panels", "preferences", "preview", "run.gui", "save", "settings",
        "tabManager", "terminal", "tooltip", "tree", "ui", "c9"
    ];
    main.provides = ["cs50.accessible.menus"];
    return main;

    function main(options, imports, register) {
        var Plugin = imports.Plugin;
        var c9 = imports.c9;
        var ui = imports.ui;
        var menus = imports.menus;
        var layout = imports.layout;
        var tabs = imports.tabManager;
        var settings = imports.settings;
        var status = imports["ace.status"];
        var basename = require("path").basename;
        var commands = imports.commands;
        var tabManager = imports.tabManager;
        var panels = imports.panels;
        var auth = imports.auth;
        var prefs = imports.preferences;

        var plugin = new Plugin("Ajax.org", main.consumes);
        var emit = plugin.getEmitter();

        // various menu objects to keep track of
        var menubar = menus.container;
        var menubarNode = menus.container.host;
        var menuChildNodes = menubarNode.childNodes;

        // mutation observers
        function handleDOMInsertion(local) {
            var menuNode = document.body;
            var baseMenuId = "cs50-menu-"
            var baseSubmenuId = "cs50-submenu-";

            // sets up each menu item added to the node
            // switch uses a regex to match the menu item class name
            var DOMInsertion = new MutationObserver(function(mutations) {
                var children = getChildNodes(mutations);
                children.forEach(function(child) {
                    var childClass = child.className;
                    switch (true) {
                        case /c9menu/.test(childClass) :
                            child.setAttribute("aria-haspopup", true);
                            child.setAttribute("role", "menu");
                            setHostDependent(child, baseMenuId);
                            handleMenuMutations(child);
                            break;
                        case /menu_item/.test(childClass) :
                            child.setAttribute("aria-disabled", /disabled/.test(child.className)),
                            child.setAttribute("aria-hidden", false);
                            child.setAttribute("role", "menuitem");
                            setHostDependent(child, baseSubmenuId);
                            addChildListeners(child, getParentNodes(child, []));
                            break;
                        case /menu_divider/.test(childClass) :
                            child.setAttribute("aria-disabled", true);
                            child.setAttribute("aria-hidden", true);
                            child.setAttribute("role", "separator");
                            break;
                    }
                });
            });

            DOMInsertion.observe(menuNode, {
                childList : true,
                subtree : true
            });
        }

        // adds observers to check open/closed state of menu and update ARIA tags
        function handleMenuMutations(menuNode) {
            var menuExpanded = new MutationObserver(function(mutations) {
                mutations.forEach(function(mutation) {
                    var host = mutation.target.host;
                    if (host.opener) {
                        visible = /Down/.test(host.opener.$ext.className);
                        host.opener.$ext.setAttribute("aria-expanded", visible);

                        host.childNodes.forEach(function(child) {
                            var childClass = child.$ext.className;
                            if (/menu_item/.test(childClass)) {
                                child.$ext.setAttribute("aria-disabled", /disabled/.test(childClass));
                            }
                        });
                    }
                });
            });

            menuExpanded.observe(menuNode, {
                attributes : true,
                attributeFilter : [ "class" ],
                subtree : true
            })
        }

        // recursively finds all relevant parent nodes for a given menu item
        // returns an array of nodes
        function getParentNodes(child, parentList) {
            switch (child.localName) {
                case "div" :
                    getParentNodes(child.parentNode.host, parentList);
                    return parentList;
                    break; // never reaches this
                case "menu" :
                    parentList.push(child.$ext);
                    getParentNodes(child.opener, parentList);
                    break;
                case "item" :
                    getParentNodes(child.parentNode, parentList);
                    break;
                case "button" :
                    parentList.push(child.$ext);
                    parentList.push(menubar);
                    break;
            }
        }

        // finds all child nodes
        // returns an array of nodes
        function getChildNodes(mutations) {
            var children = [];
            mutations.forEach(function(mutation) {
                for (var i = 0; i < mutation.addedNodes.length; ++i) {
                    children.push(mutation.addedNodes[i]);
                }
            });
            return children;
        }

        // set any attributes that depend on waiting for the $host attribute of
        // a node to be loaded
        function setHostDependent(node, baseString) {
            var check_host = window.setInterval(function() {
                if (typeof node.host !== "undefined") {
                    clearInterval(check_host);
                    node.setAttribute("id", baseString + node.host.$uniqueId);
                    if (typeof node.host.submenu !== "undefined") {
                        node.setAttribute("aria-haspopup", true);
                    }
                }
            }, 100);
        }

        // adds all event listeners for a given child and its parents
        function addChildListeners(child, parents) {
            child.addEventListener("mouseout", function(event) {
                parents.forEach(function(parent) {
                    parent.removeAttribute("aria-activedescendant");
                    parent.removeAttribute("aria-owns");
                });
            });

            child.addEventListener("mouseover", function(event) {
                parents.forEach(function(parent) {
                    parent.setAttribute("aria-activedescendant", child.id);
                    if (/c9-menu-btn/.test(parent.className)) {
                        parent.setAttribute("aria-owns", child.id);
                    }
                });
            });
        }

        // populate the menus that are in the main menubar on page load
        function populateMenus() {
            var baseMenuId = "cs50-menu-"

            // hide the minimize button
            menubar.setAttribute("role", "menubar");

            for (var i = 0; i < menuChildNodes.length; ++i) {
                var childNode = menuChildNodes[i];
                var child = childNode.$ext;
                var childClass = child.getAttribute("class");
                setHostDependent(child, baseMenuId);

                switch (true) {
                    case /c9-menu-btn/.test(childClass) :
                        child.setAttribute("aria-disabled", false);
                        child.setAttribute("aria-expanded", false);
                        child.setAttribute("aria-haspopup", true);
                        child.setAttribute("role", "menuitem");
                        break;
                    case /c9-mbar-minimize/.test(childClass) :
                        childNode.disable();
                        childNode.hide();
                        break;
                }
                addChildListeners(child, [ childNode.$pHtmlNode ]);
            }
        }

        function load() {
            handleDOMInsertion(this);
            populateMenus();
        }

        /***** Methods *****/



        /***** Lifecycle *****/

        plugin.on("load", function() {
            load();
        });
        plugin.on("unload", function() {

        });

        /***** Register and define API *****/

        plugin.freezePublicAPI({

        });

        register(null, {
            "cs50.accessible.menus": plugin
        });
    }
});